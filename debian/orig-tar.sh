#!/bin/sh -e

VERSION=$2
TAR=../netbeans-cvsclient_$VERSION.orig.tar.gz
DIR=netbeans-cvsclient-$VERSION
TAG=$(echo release${VERSION}_base | sed -e's/\.//')

hg clone http://hg.netbeans.org/main/file/$TAG/lib.cvsclient $DIR.tmp
cd $DIR.tmp
hg update $TAG
cd ..
mkdir $DIR
mv $DIR.tmp/lib.cvsclient/* $DIR
rm -r $DIR.tmp
tar -c -z -f $TAR $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi
